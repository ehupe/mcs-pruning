import os

def runPHEVTests():
    #systems   = ["RTS79", "RTS79_2800", "RTS79_2700", "RTS79_2600", "RTS79_2500", "RTS79_2400", "RTS79_2300", "RTS79_2200", "RTS79_2100" ]
    systems   = ["RTS79" ]
    placement = ["EvenAllBuses", "EvenLoadBuses", "FairDistribution"]
    #placement = ["FairDistribution"]
    #lines     = ["--useLines", ""]
    lines     = [""]
    pLevels   = [1.0, 0.95, 0.9, 0.85, 0.8, 0.75, 0.7, 0.65, 0.6, 0.55, 0.5, 0.45, 0.4, 0.35, 0.3, 0.25, 0.2, 0.15, 0.14, 0.13, 0.12, 0.11, 0.1, 0.09, 0.08, 0.07, 0.06, 0.05, 0.04, 0.03, 0.02, 0.01, 0.009, 0.008, 0.007, 0.006, 0.005, 0.004, 0.003, 0.002, 0.001]


    command  = "./MCS_Pruning --system %s --usePHEVs --placement %s --trials 10 --penetration %.3f %s"

    for system in systems:
        for p in placement:
            for l in lines:
                for pl in pLevels:
                    curCommand = command % (system, p, pl, l)
                    print curCommand
                    os.system(curCommand)



def runOpenMPTests():
    systems    = ["RTS79", "MRTS", "RTS96"]
    batchSizes = xrange(1000, 10100, 500)
    numThreads = xrange(1, 13, 1)

    command    = "./MCS\ Pruning --system %s --sampler MCS_OMP --numThreads %i --batchSize %i --trials 10"
    for s in systems:
        # Baseline
        curCommand = "./MCS\ Pruning --system %s --trials 10" % (s)
        os.system(curCommand)

        for t in numThreads:
            for bs in batchSizes:
                curCommand = command % (s, t, bs)
                os.system(curCommand)
                print ""

if __name__ == "__main__":

    #runOpenMPTests()
    runPHEVTests()
