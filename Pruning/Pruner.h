/*
 * Pruner.h
 *
 *  Created on: Nov 11, 2010
 *      Author: rgreen
 */

#ifndef PRUNER_H_
#define PRUNER_H_

#include <vector>
#include <map>
#include <omp.h>
#include <string>

#include "Classifier.h"
#include "CStopWatch.h"
#include "defs.h"
#include "Generator.h"
#include "Line.h"
#include "MTRand.h"
#include "Utils.h"

class Pruner {
    public:
        Pruner(int np, int nt, std::vector<Generator> g, std::vector<Line> l, Classifier* opf, double pload, bool ul=false);
        ~Pruner();

        virtual void Prune(MTRand& mt) = 0;
        void clearVectors();
        void clearTimes();

        void Init(int np, int nt, std::vector<Generator> g, std::vector<Line> l, Classifier* opf, double pload, bool ul=false);

        virtual void Reset(int np, int nt);
        double getPruningTime();
        std::map<std::string, double> getSuccessStates();
        std::map<std::string, double> getFailureStates();

        double EvaluateSolution(std::vector<int> curSolution, bool localSearch = true);
        double EvaluateSolution(std::string curSolution,  bool localSearch = true);

        double getTotalIterations();
        double getStatesSlope();
        double getProbSlope();
        double getPrunedProb();
        double getFailedProb();

        void setObjective(pruning::PRUNING_OBJECTIVE p);
        void setuseLocalSearch(bool ul);
        void setLineAdjustment(double la);

        void setTotalVehicles(int tv);
        void setPenetrationLevel(double p);
        void setNumBuses(int nb);
        void setRho(double p);
        void setUsePHEVs(bool ul);
        void setPHEVPlacement(pruning::PHEV_PLACEMENT p);
        void setUseLogging(bool u);
        void setNegateFitness(bool b);
        void setUseLines(bool ul);
        void setUseLocalSearch(bool uls);
        void setNumThreads(int i);

        void setStoppingMethod(pruning::STOPPING_METHOD s);
        void setStoppingValue(double sv);

        void setMinStatesSlope(double s);
        void setMaxStatesPruned(double s);
        void setMaxProbPruned(double p);
        void setMinProbSlope(double p);
        void setMaxGenerations(int i);
        void setPermuteSolutions(bool b);

        void writeLog(std::string fileName);

    protected:
        
        virtual bool isConverged();
        bool 	useLogging;
        bool 	useLocalSearch, usePHEVs;
        bool 	negateFitness;
        bool 	useLines;
        bool    permuteSolutions;
        
        double 	pruningTime,
                pLoad, oTime, searchTime;
        double	lineAdjustment;
        double  minSlope, maxProb, maxStatesPruned;
        double 	prunedProb, numStatesPruned;
        double  avgStatesPruned, avgProbPruned, avgStatesFailed;
        double  minProbSlope;
        int 	Np, Nd, Nt, totalIterations, collisions;
        int     numThreads;

        std::map<std::string, double> 	successStates;
        std::map<std::string, double> 	failureStates;
        std::map<std::string, int> 		sampledStates;

        std::vector<Generator> 	gens;
        std::vector<Line> 		lines;

        std::vector<int>		fCollisions;
        std::vector<double> 	vAvgStatesPruned, 	vAvgProbPruned, 	vAvgFailedStates,
                                statesPruned,		fStatesPruned, 	  	probPruned,
                                stdDevFitness,      hammingDiversity;

        std::vector < std::vector < double > > R, Rbest; // Personal Position        
        std::vector < double > M, Mbest;                 // Fitness
        std::vector < double > G;        // Global Best Position        

        Classifier* classifier;
        CStopWatch timer, subTimer;

        pruning::PHEV_PLACEMENT 	phevPlacement;
        pruning::PRUNING_OBJECTIVE 	pruningObj;
        pruning::STOPPING_METHOD 	stoppingMethod;

        double 	penetrationLevel, CDD, rho;
        int 	totalVehicles, numClasses, numBuses;
};

#endif /* PRUNER_H_ */
