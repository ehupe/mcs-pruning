#include "Logging.h"
#include <iostream>
#include "Eigen/Dense"

namespace Logging {

	void writeLineOutages(std::string root, std::string curSystem, std::string samplingMethod, std::string pruningMethod, std::string classificationMethod,
		std::string pruningObj, bool useLines, bool multiObj, char* aTime, bool useLocalSearch, bool usePHEVs, bool negateFitness, double penetrationLevel,
		pruning::PHEV_PLACEMENT phevPlacement,
		std::vector < std::vector < int > > lineOutages, std::vector<Line> lines, int numThreads) {


		std::stringstream   ss;
		std::ofstream       myFile;
		std::string         fName;

		ss << getBaseFileName(root, curSystem, samplingMethod, pruningMethod, classificationMethod, pruningObj, useLines, multiObj, useLocalSearch, usePHEVs,
			negateFitness, penetrationLevel, phevPlacement, numThreads);


		ss << "_" << aTime << "_LineOutageCounts.csv";
		ss >> fName;

		myFile.open(fName.c_str(), std::ios::trunc);
		for (unsigned int i = 0; i<lines.size(); i++) {
			myFile << i << " ";
		}
		myFile << std::endl;
		for (unsigned int i = 0; i<lineOutages.size(); i++) {
			for (unsigned int j = 0; j< lineOutages[i].size(); j++) {
				myFile << lineOutages[i][j] << " ";
			}
			myFile << std::endl;
		}
		myFile.close();
	}

	void writeGeneratorOutages(std::string root, std::string curSystem, std::string samplingMethod, std::string pruningMethod, std::string classificationMethod,
		std::string pruningObj, bool useLines, bool multiObj, char* aTime, bool useLocalSearch, bool usePHEVs, bool negateFitness, double penetrationLevel,
		pruning::PHEV_PLACEMENT phevPlacement, std::vector < std::vector < int > > genOutageCounts, std::vector<Generator> gens, int numThreads) {
		std::stringstream   ss;
		std::ofstream       myFile;
		std::string         fName;

		ss << getBaseFileName(root, curSystem, samplingMethod, pruningMethod, classificationMethod, pruningObj, useLines, multiObj, useLocalSearch, usePHEVs,
			negateFitness, penetrationLevel, phevPlacement, numThreads);
		ss << "_" << aTime << "_GeneratorOutageCounts.csv";
		ss >> fName;
		myFile.open(fName.c_str(), std::ios::trunc);
		for (unsigned int i = 0; i<gens.size(); i++) {
			myFile << i << " ";
		}
		myFile << std::endl;
		for (unsigned int i = 0; i<genOutageCounts.size(); i++) {
			for (unsigned int j = 0; j< genOutageCounts[i].size(); j++) {
				myFile << genOutageCounts[i][j] << " ";
			}
			myFile << std::endl;
		}
		myFile.close();
	}

	std::string getFileName(std::string root, std::string curSystem, std::string samplingMethod, std::string pruningMethod, std::string classificationMethod,
		std::string pruningObj, bool useLines, bool multiObj, char* aTime, bool useLocalSearch, bool usePHEVs, bool negateFitness, double penetrationLevel,
		pruning::PHEV_PLACEMENT phevPlacement, int numThreads) {

		std::stringstream ss(std::stringstream::in | std::stringstream::out);
		std::string fName;

		ss << getBaseFileName(root, curSystem, samplingMethod, pruningMethod, classificationMethod, pruningObj, useLines, multiObj, useLocalSearch, usePHEVs,
			negateFitness, penetrationLevel, phevPlacement, numThreads);
		ss << "_" << aTime << ".csv";

		ss >> fName;
		return fName;
	}

	std::string getBaseFileName(std::string root, std::string curSystem, std::string samplingMethod, std::string pruningMethod, std::string classificationMethod,
		std::string pruningObj, bool useLines, bool multiObj, bool useLocalSearch, bool usePHEVs, bool negateFitness, double penetrationLevel,
		pruning::PHEV_PLACEMENT phevPlacement, int numThreads) {

		std::stringstream   ss;
		std::string fName;

		ss << root << "/" << curSystem << "/";
		ss << samplingMethod << "_"
			<< pruningMethod << "_"
			<< curSystem << "_"
			<< pruningObj;

		if (useLines) ss << "_LineFailures";
		else         ss << "_NoLineFailures";

		if (multiObj) ss << "_MultiObj";
		else         ss << "_SingleObj";

		ss << "_" << classificationMethod;

		if (useLocalSearch) ss << "_LocalSearch";

		if (negateFitness) ss << "_NegObj";

		if (usePHEVs) {
			ss << "_PHEVs";
			ss << "_" << getPHEVPlacementString(phevPlacement);
			ss << "_" << std::fixed << std::setprecision(3) << penetrationLevel;
		}
		ss << "_OMP_" << numThreads;

		ss >> fName;
		return fName;
	}

	void getTimeStamp(char* aTime) {
		time_t now;
		//char* aTime = new char[20];

		time(&now);
		struct tm * timeinfo = localtime(&now);;
		strftime(aTime, 20, "%m_%d_%Y_%H_%M_%S", timeinfo);

		//return aTime;
	}
}