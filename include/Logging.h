#ifndef LOGGING_H_
#define LOGGING_H_

#include <iomanip>
#include <iterator>
#include <math.h>
#include <sstream>
#include <stdio.h>
#include <string>
#include <vector>

#ifndef _OPENMP
#include "omp.h"
#endif

#include "anyoption.h"
#include "Bus.h"
#include "defs.h"
#include "Generator.h"
#include "Line.h"
#include "MTRand.h"
#include "Primes.h"
#include "RandomNumbers.h"

namespace Loggging {

	extern void writeLineOutages(std::string root, std::string curSystem, std::string method, std::string pruningMethod,
		std::string classificationMethod, std::string pruningObj,
		bool useLines, bool multiObj, char* aTime,
		bool useLocalSearch, bool usePHEVs, bool negateFitness, double penetrationLevel, pruning::PHEV_PLACEMENT phevPlacement,
		std::vector < std::vector < int > > lineOutageCounts, std::vector<Line> lines, int numThreads);

	extern void writeGeneratorOutages(std::string root, std::string curSystem, std::string method, std::string pruningMethod,
		std::string classificationMethod, std::string pruningObj, bool useLines, bool multiObj, char* aTime,
		bool useLocalSearch, bool usePHEVs, bool negateFitness, double penetrationLevel, pruning::PHEV_PLACEMENT phevPlacement,
		std::vector < std::vector < int > > genOutageCounts, std::vector<Generator> gens, int numThreads);

	extern std::string getBaseFileName(std::string root, std::string curSystem, std::string samplingMethod, std::string pruningMethod,
		std::string classificationMethod, std::string pruningObj,
		bool useLines, bool multiObj, bool useLocalSearch, bool usePHEVs, bool negateFitness, double penetrationLevel, pruning::PHEV_PLACEMENT phevPlacement,
		int numThreads);

	extern std::string getFileName(std::string root, std::string curSystem, std::string samplingMethod, std::string pruningMethod,
		std::string classificationMethod, std::string pruningObj,
		bool useLines, bool multiObj, char* aTime, bool useLocalSearch, bool usePHEVs, bool negateFitness,
		double penetrationLevel, pruning::PHEV_PLACEMENT phevPlacement, int numThreads);

	//extern char* getTimeStamp();
	extern void getTimeStamp(char* aTime);
};
#endif

