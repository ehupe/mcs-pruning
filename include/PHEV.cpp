#include "Vector.h"
#include <iostream>
#include "Eigen/Dense"

namespace PHEV {

	void calculatePHEVLoad(
		double penetrationLevel, double rho, int totalVehicles, int numBuses,
		std::vector<double>& phevLoad, std::vector<double>& phevGen, MTRand& mt, pruning::PHEV_PLACEMENT phevPlacement) {

		double mean, stdDev;
		double tempK, tempBC;
		double iStar;
		double chargeDischargeDistance, numClasses;
		std::vector<double> numPHEVs;
		std::vector<double> Ae, Be, BC, K,
			meanK, meanBC, sigmaK, sigmaBC,
			kMin, kMax, bcMin, bcMax,
			vehicleProbs, milesDriven,
			neededEnergy, energyPerMile,
			departureTime, arrivalTime,
			rechargeLength, rechargeVoltage,
			rechargeCurrent, rechargeTime,
			loadPerClass;
		std::vector < std::vector <double> > epsilon;

		numClasses = 4, chargeDischargeDistance = 40;

		numPHEVs.resize(numClasses, 0);
		meanK.resize(numClasses, 0);         meanBC.resize(numClasses, 0);
		sigmaK.resize(numClasses, 0);         sigmaBC.resize(numClasses, 0);
		kMin.resize(numClasses, 0);         kMax.resize(numClasses, 0);
		bcMin.resize(numClasses, 0);         bcMax.resize(numClasses, 0);
		BC.resize(numClasses, 0);         K.resize(numClasses, 0);
		Ae.resize(numClasses, 0);         Be.resize(numClasses, 0);

		vehicleProbs.resize(numClasses, 0);
		milesDriven.resize(numClasses, 0);
		neededEnergy.resize(numClasses, 0);
		energyPerMile.resize(numClasses, 0);
		departureTime.resize(numClasses, 0);
		arrivalTime.resize(numClasses, 0);
		rechargeLength.resize(numClasses, 0);
		rechargeVoltage.resize(numClasses, 0);
		rechargeCurrent.resize(numClasses, 0);
		rechargeTime.resize(numClasses, 0);
		loadPerClass.resize(numClasses, 0);
		epsilon.resize(numClasses, std::vector<double>(numClasses, 0));

		vehicleProbs[0] = .2; vehicleProbs[1] = .3;
		vehicleProbs[2] = .3; vehicleProbs[3] = .2;

		Ae[0] = 0.3790; Ae[1] = 0.4288; Ae[2] = 0.6720; Ae[3] = 0.8180;
		Be[0] = 0.4541; Be[1] = 0.4179; Be[2] = 0.4040; Be[3] = 0.4802;
		kMin[0] = .2447;  kMin[1] = .2750;  kMin[2] = .3217;  kMin[3] = .3224;
		kMax[0] = 0.5976; kMax[1] = 0.6151; kMax[2] = 0.5428; kMax[3] = 0.4800;
		bcMax[0] = 12;     bcMax[1] = 14;     bcMax[2] = 21;     bcMax[3] = 23;
		bcMin[0] = 8;      bcMin[1] = 10;     bcMin[2] = 17;     bcMin[3] = 19;

		for (int i = 0; i<numClasses; i++) {
			meanK[i] = (kMax[i] + kMin[i]) / 2;
			meanBC[i] = (bcMax[i] + bcMin[i]) / 2;
			sigmaK[i] = (kMax[i] - kMin[i]) / 4;
			sigmaBC[i] = (bcMax[i] - bcMin[i]) / 4;
		}

		// During Each MCS Iteration
		for (int i = 0; i<numClasses; i++) {
			// Calculate Number of PHEVS per Class
			mean = totalVehicles*penetrationLevel*vehicleProbs[i];
			stdDev = .01 * mean;
			numPHEVs[i] = mt.randNorm(mean, stdDev);

			//Calculate total # of V2G?
			epsilon[0][0] = sigmaK[i] * sigmaK[i];
			epsilon[0][1] = rho*sigmaK[i] * sigmaBC[i];
			epsilon[1][0] = rho*sigmaK[i] * sigmaBC[i];
			epsilon[1][1] = sigmaBC[i] * sigmaBC[i];
			Utils::twoByTwoCholeskyDecomp(epsilon);

			// Move the following to invidiual cars
			//gsl_ran_bivariate_gaussian(r, sigmaK[i], sigmaBC[i], rho, &tempK, &tempBC);
			RandomNumbers::bivariateGaussian(mt, sigmaK[i], sigmaBC[i], rho, tempK, tempBC);
			K[i] = meanK[i] + tempK;
			BC[i] = meanBC[i] + tempBC;

			// Calculate Miles driven per class
			//milesDriven[i] = gsl_ran_lognormal(r, 3.37, 0.5);
			milesDriven[i] = RandomNumbers::logNormal(mt, 3.37, 0.5);

			//Arrival and Departure Time
			departureTime[i] = mt.randNorm(7, sqrt(3.0));
			arrivalTime[i] = mt.randNorm(18, sqrt(3.0));

			// Energy Needed
			if (milesDriven[i] > chargeDischargeDistance) { // full Charge Required
				neededEnergy[i] = BC[i];
			}
			else {
				neededEnergy[i] = (milesDriven[i] * (Ae[i] * pow(K[i], Be[i])));
			}

			// Compute Recharge Length
			rechargeLength[i] = (24.0 - arrivalTime[i]) + departureTime[i];

			// Compute Voltage needed
			if (mt.rand() < 0.70) {
				rechargeVoltage[i] = 120;
				iStar = 15;
			}
			else {
				rechargeVoltage[i] = 240;
				iStar = 30;
			}
			// Compute current needed
			rechargeCurrent[i] = std::min((milesDriven[i] * 1000) / (rechargeVoltage[i] * rechargeLength[i]), iStar);

			// Compute total power in MW in a single hour
			loadPerClass[i] = (numPHEVs[i] * rechargeVoltage[i] * rechargeCurrent[i]) / 1000000;
		}

		//Distributed Load
		phevLoad.clear(); phevGen.clear();
		phevLoad.resize(numBuses, 0); phevGen.resize(numBuses, 0);

		if (phevPlacement == pruning::PP_EVEN_ALL_BUSES) {
			//Evenly at All Buses
			for (int i = 0; i<numBuses; i++) {
				phevLoad[i] = 0;
				for (int j = 0; j<numClasses; j++) {
					phevLoad[i] += loadPerClass[j] / (double)numBuses;
				}
			}
		}
		else if (phevPlacement == pruning::PP_EVEN_LOAD_BUSES) {
			// Currently handles only RTS79 with 18 Buses
			for (int j = 0; j<numClasses; j++) {
				phevLoad[0] += loadPerClass[j] / 18.0;
				phevLoad[1] += loadPerClass[j] / 18.0;
				phevLoad[2] += loadPerClass[j] / 18.0;
				phevLoad[3] += loadPerClass[j] / 18.0;
				phevLoad[4] += loadPerClass[j] / 18.0;
				phevLoad[5] += loadPerClass[j] / 18.0;
				phevLoad[6] += loadPerClass[j] / 18.0;
				phevLoad[7] += loadPerClass[j] / 18.0;
				phevLoad[8] += loadPerClass[j] / 18.0;
				phevLoad[9] += loadPerClass[j] / 18.0;
				phevLoad[10] += loadPerClass[j] / 18.0;
				phevLoad[13] += loadPerClass[j] / 18.0;
				phevLoad[14] += loadPerClass[j] / 18.0;
				phevLoad[15] += loadPerClass[j] / 18.0;
				phevLoad[16] += loadPerClass[j] / 18.0;
				phevLoad[18] += loadPerClass[j] / 18.0;
				phevLoad[19] += loadPerClass[j] / 18.0;
				phevLoad[20] += loadPerClass[j] / 18.0;
			}
		}
		else if (phevPlacement == pruning::PP_RANDOM_ALL_BUSES) {
			//Evenly at All Buses
			for (int i = 0; i<numBuses; i++) {
				phevLoad[i] = 0;
				for (int j = 0; j<numClasses; j++) {
					phevLoad[i] += loadPerClass[j] / (double)numBuses * mt.rand();
				}
			}
		}
		else if (phevPlacement == pruning::PP_RANDOM_LOAD_BUSES) {
			for (int j = 0; j<numClasses; j++) {
				phevLoad[0] += loadPerClass[j] / 18.0 * mt.rand();
				phevLoad[1] += loadPerClass[j] / 18.0 * mt.rand();
				phevLoad[2] += loadPerClass[j] / 18.0 * mt.rand();
				phevLoad[3] += loadPerClass[j] / 18.0 * mt.rand();
				phevLoad[4] += loadPerClass[j] / 18.0 * mt.rand();
				phevLoad[5] += loadPerClass[j] / 18.0 * mt.rand();
				phevLoad[6] += loadPerClass[j] / 18.0 * mt.rand();
				phevLoad[7] += loadPerClass[j] / 18.0 * mt.rand();
				phevLoad[8] += loadPerClass[j] / 18.0 * mt.rand();
				phevLoad[9] += loadPerClass[j] / 18.0 * mt.rand();
				phevLoad[10] += loadPerClass[j] / 18.0 * mt.rand();
				phevLoad[13] += loadPerClass[j] / 18.0 * mt.rand();
				phevLoad[14] += loadPerClass[j] / 18.0 * mt.rand();
				phevLoad[15] += loadPerClass[j] / 18.0 * mt.rand();
				phevLoad[16] += loadPerClass[j] / 18.0 * mt.rand();
				phevLoad[18] += loadPerClass[j] / 18.0 * mt.rand();
				phevLoad[19] += loadPerClass[j] / 18.0 * mt.rand();
				phevLoad[20] += loadPerClass[j] / 18.0 * mt.rand();
			}
		}
		else if (phevPlacement == pruning::PP_FAIR_DISTRIBUTION) {

			int TotalNumbOfCustomers = 1078800;
			double percentPerBus;
			for (int j = 0; j<numClasses; j++) {
				percentPerBus = 3.5 / 100.0;
				phevLoad[0] += (loadPerClass[j] * percentPerBus);

				percentPerBus = 3.3 / 100.0;
				phevLoad[1] += (loadPerClass[j] * percentPerBus);

				percentPerBus = 7.7 / 100.0;
				phevLoad[2] += (loadPerClass[j] * percentPerBus);

				percentPerBus = 1.2 / 100.0;
				phevLoad[3] += (loadPerClass[j] * percentPerBus);

				percentPerBus = 1.9 / 100.0;
				phevLoad[4] += (loadPerClass[j] * percentPerBus);

				percentPerBus = 5.6 / 100.0;
				phevLoad[5] += (loadPerClass[j] * percentPerBus);

				percentPerBus = 4.4 / 100.0;
				phevLoad[6] += (loadPerClass[j] * percentPerBus);

				percentPerBus = 7.2 / 100.0;
				phevLoad[7] += (loadPerClass[j] * percentPerBus);

				percentPerBus = 8.2 / 100.0;
				phevLoad[8] += (loadPerClass[j] * percentPerBus);

				percentPerBus = 9.2 / 100.0;
				phevLoad[9] += (loadPerClass[j] * percentPerBus);

				percentPerBus = 12.1 / 100.0;
				phevLoad[10] += (loadPerClass[j] * percentPerBus);

				percentPerBus = 6.4 / 100.0;
				phevLoad[13] += (loadPerClass[j] * percentPerBus);

				percentPerBus = 7.9 / 100.0;
				phevLoad[14] += (loadPerClass[j] * percentPerBus);

				percentPerBus = 2.9 / 100.0;
				phevLoad[15] += (loadPerClass[j] * percentPerBus);

				percentPerBus = 8.0 / 100.0;
				phevLoad[16] += (loadPerClass[j] * percentPerBus);

				percentPerBus = 6.0 / 100.0;
				phevLoad[18] += (loadPerClass[j] * percentPerBus);

				percentPerBus = 3.6 / 100.0;
				phevLoad[19] += (loadPerClass[j] * percentPerBus);

			}
		}

		//Scaling for Model
		for (int i = 0; i<numBuses; i++) {
			phevLoad[i] /= 100;
		}

		numPHEVs.clear();
		meanK.clear(); meanBC.clear();
		sigmaK.clear(); sigmaBC.clear();
		kMin.clear(); kMax.clear();
		bcMin.clear(); bcMax.clear();
		BC.clear(); K.clear();
		Ae.clear(); Be.clear();
		vehicleProbs.clear();
		milesDriven.clear(); neededEnergy.clear();
		energyPerMile.clear(); epsilon.clear();
		departureTime.clear(); arrivalTime.clear();
		rechargeLength.clear();
		rechargeVoltage.clear(); rechargeCurrent.clear();
		rechargeTime.clear();   loadPerClass.clear();
		epsilon.clear();

		//gsl_rng_free (r);
	}
}