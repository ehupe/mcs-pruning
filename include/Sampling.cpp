#include "Sampling.h"
#include <iostream>
#include "Eigen/Dense"

namespace Sampling {

	// N - Number of points
	// D - Dimension
	std::vector < std::vector < double > > sobol_points(unsigned N, unsigned D) {
		std::vector < std::vector < double > > POINTS(N, vector<double>(D, 0.0));

		//ifstream infile("E://Visual Studio 2010//Projects//MCS Pruning//MCS Pruning//Files//joe-kuo-old.1111", ios::in);
		//ifstream infile("E://Visual Studio 2010//Projects//MCS Pruning//MCS Pruning//Files//joe-kuo-other-0.7600", ios::in);
		//ifstream infile("E://Visual Studio 2010//Projects//MCS Pruning//MCS Pruning//Files//joe-kuo-other-2.3900", ios::in);
		//ifstream infile("E://Visual Studio 2010//Projects//MCS Pruning//MCS Pruning//Files//joe-kuo-other-3.7300", ios::in);
		ifstream infile("E://Visual Studio 2010//Projects//MCS Pruning//MCS Pruning//Files//joe-kuo-other-4.5600", ios::in);
		if (!infile) {
			cout << "Input file containing direction numbers cannot be found!\n";
			exit(1);
		}
		char buffer[1000];
		infile.getline(buffer, 1000, '\n');

		// L = max number of bits needed 
		unsigned L = (unsigned)ceil(log((double)N) / log(2.0));

		// C[i] = index from the right of the first zero bit of i
		unsigned *C = new unsigned[N];
		C[0] = 1;
		for (unsigned i = 1; i <= N - 1; i++) {
			C[i] = 1;
			unsigned value = i;
			while (value & 1) {
				value >>= 1;
				C[i]++;
			}
		}

		// POINTS[i][j] = the jth component of the ith point
		//                with i indexed from 0 to N-1 and j indexed from 0 to D-1
		/*double **POINTS = new double * [N];
		for(unsigned i=0;i<N;i++){ POINTS[i] = new double [D];}
		for(unsigned j=0;j<D;j++){ POINTS[0][j] = 0; }*/

		// ----- Compute the first dimension -----

		// Compute direction numbers V[1] to V[L], scaled by pow(2,32)
		unsigned *V = new unsigned[L + 1];
		for (unsigned i = 1; i <= L; i++) {
			V[i] = 1 << (32 - i);
			// cout << L << " " << N << " " << i << ": " << V[i] << "\n";
		} // all m's = 1

		  // Evalulate X[0] to X[N-1], scaled by pow(2,32)
		unsigned *X = new unsigned[N];
		X[0] = 0;
		for (unsigned i = 1; i <= N - 1; i++) {

			X[i] = X[i - 1] ^ V[C[i - 1]];
			// cout << C[i-1] << " " << V[C[i-1]] << " " << X[i] << endl;
			POINTS[i][0] = (double)X[i] / pow(2.0, 32); // *** the actual points
														//        ^ 0 for first dimension
		}

		// Clean up
		delete[] V;
		delete[] X;

		// ----- Compute the remaining dimensions -----
		for (unsigned j = 1; j <= D - 1; j++) {

			// Read in parameters from file 
			unsigned d, s;
			unsigned a;
			infile >> d >> s >> a;
			unsigned *m = new unsigned[s + 1];
			for (unsigned i = 1; i <= s; i++) infile >> m[i];

			// Compute direction numbers V[1] to V[L], scaled by pow(2,32)
			unsigned *V = new unsigned[L + 1];
			if (L <= s) {
				for (unsigned i = 1; i <= L; i++) V[i] = m[i] << (32 - i);
			}
			else {
				for (unsigned i = 1; i <= s; i++) V[i] = m[i] << (32 - i);
				for (unsigned i = s + 1; i <= L; i++) {
					V[i] = V[i - s] ^ (V[i - s] >> s);
					for (unsigned k = 1; k <= s - 1; k++) {
						V[i] ^= (((a >> (s - 1 - k)) & 1) * V[i - k]);
					}
				}
			}

			// Evalulate X[0] to X[N-1], scaled by pow(2,32)
			unsigned *X = new unsigned[N];
			X[0] = 0;
			for (unsigned i = 1; i <= N - 1; i++) {
				X[i] = X[i - 1] ^ V[C[i - 1]];
				POINTS[i][j] = (double)X[i] / pow(2.0, 32); // *** the actual points
															//        ^ j for dimension (j+1)
			}

			// Clean up
			delete[] m;
			delete[] V;
			delete[] X;
		}
		delete[] C;

		return POINTS;
	}

	// N - Point Number
	// D - Dimension
	double *single_sobol_point(unsigned N, unsigned D) {

		ifstream infile("E://Visual Studio 2010//Projects//MCS Pruning//MCS Pruning//Files//joe-kuo-old.111", ios::in);
		/*ifstream infile("E://Visual Studio 2010//Projects//MCS Pruning//MCS Pruning//Files//joe-kuo-other-0.7600", ios::in);
		ifstream infile("E://Visual Studio 2010//Projects//MCS Pruning//MCS Pruning//Files//joe-kuo-old-2.3900", ios::in);
		ifstream infile("E://Visual Studio 2010//Projects//MCS Pruning//MCS Pruning//Files//joe-kuo-old-3.7300", ios::in);
		ifstream infile("E://Visual Studio 2010//Projects//MCS Pruning//MCS Pruning//Files//joe-kuo-old-4.5600", ios::in);*/
		if (!infile) {
			cout << "Input file containing direction numbers cannot be found!\n";
			exit(1);
		}
		// Gets rid of first line only
		char buffer[1000];
		infile.getline(buffer, 1000, '\n');

		// L = max number of bits needed 
		unsigned L = (unsigned)ceil(log((double)N) / log(2.0));

		// C[i] = index from the right of the first zero bit of i
		// for example 3 in binary 0011, so c[3]=3, which is the index of first zero from right
		// result of calculation is stored in an [N] array  
		unsigned *C = new unsigned[N];
		C[0] = 1;
		for (unsigned i = 1; i <= N - 1; i++) {
			C[i] = 1;
			unsigned value = i;
			while (value & 1) {
				value >>= 1;
				C[i]++;
			}
		}

		double *POINT = new double[D];
		for (unsigned j = 0; j<D; j++) {
			POINT[j] = 0;
		}

		// Compute direction numbers V[1] to V[L], scaled by pow(2,32)
		unsigned *V = new unsigned[L + 1];
		for (unsigned i = 1; i <= L; i++) {
			V[i] = 1 << (32 - i);
		} // all m's = 1

		  // Compute first dimension of Nth row
		  // Evalulate N-1th X from 0, scaled by pow(2,32), keep changing value of X
		  // N must be int >=1
		unsigned X = 0;
		for (unsigned i = 1; i <= N - 1; i++) {
			X = X ^ V[C[i - 1]];
		}
		POINT[0] = (double)X / pow(2.0, 32);
		//    ^ first dimension of Nth row 

		// Clean up
		delete[] V;

		// ----- Compute the remaining dimensions ----- j is [1, D-1]
		for (unsigned j = 1; j <= D - 1; j++) {

			// Read in parameters from file 
			unsigned d, s;
			unsigned a;
			infile >> d >> s >> a;
			unsigned *m = new unsigned[s + 1];
			for (unsigned i = 1; i <= s; i++) infile >> m[i];

			// Compute direction numbers V[1] to V[L], scaled by pow(2,32)
			unsigned *V = new unsigned[L + 1];
			if (L <= s) {
				for (unsigned i = 1; i <= L; i++) V[i] = m[i] << (32 - i);
			}
			else {
				for (unsigned i = 1; i <= s; i++) V[i] = m[i] << (32 - i);
				for (unsigned i = s + 1; i <= L; i++) {
					V[i] = V[i - s] ^ (V[i - s] >> s);
					for (unsigned k = 1; k <= s - 1; k++) {
						V[i] ^= (((a >> (s - 1 - k)) & 1) * V[i - k]);
					}
				}
			}

			// Evalulate X[0] to X[N-1], scaled by pow(2,32)    
			unsigned X = 0;
			for (unsigned i = 1; i <= N - 1; i++) {
				X = X ^ V[C[i - 1]];

			}
			POINT[j] = (double)X / pow(2.0, 32); // *** the actual points
												 //        ^ j for dimension (j+1)
												 // Clean up
			delete[] m;
			delete[] V;
		}
		delete[] C;

		return POINT;
	}

	std::vector<double> piNumbers;
	int timesPiNumberCalled = 0;
	double piNumber(int Ns) {

		// Empty on First Call
		if (piNumbers.empty()) {
			std::ifstream piFin;
			// double pid;
			string piVal;

			if (!piFin.is_open()) {
				try {
					piFin.open("E://Visual Studio 2010//Projects//MCS Pruning//MCS Pruning//Files//piNumbers.txt");
				}
				catch (std::ifstream::failure e) {
					std::cerr << "Open piNumbers.txt Failed\n";
				}
			}

			if (piFin.is_open()) {
				while (!piFin.eof()) {
					std::getline(piFin, piVal);
					piNumbers.push_back(std::atof(piVal.c_str()));
				}
			}
		}

		if (timesPiNumberCalled >= (int)piNumbers.size()) {
			timesPiNumberCalled = 0;
		}

		return piNumbers[timesPiNumberCalled++];
	}

	std::vector < std::vector < double > > latinHyperCube_Random(int Nv, int Ns, MTRand& mt)
	{

		std::vector < std::vector < double > > lhsMatrix(Ns, vector<double>(Nv, 0));
		double segmentSize = 1.0 / (float)Ns;
		double curSegment = 0.0;
		int a;
		double c;

		// Create Initial Matrix
		for (int i = 0; i < Ns; i++) {
			for (int j = 0; j < Nv; j++) {
				lhsMatrix[i][j] = ((i + 1) - 1 + mt.rand()) / Ns;
			}
			curSegment += segmentSize;
		}

		// Randomly match
		for (int i = 0; i < Nv; i++) {
			for (int j = 0; j < Ns; j++) {
				a = mt.randInt(Ns - 1);
				c = lhsMatrix[j][i];
				lhsMatrix[j][i] = lhsMatrix[a][i];
				lhsMatrix[a][i] = c;
			}
		}

		return lhsMatrix;
	}

	std::vector < std::vector < double > > descriptiveSampling_Random(int Nv, int Ns, MTRand& mt) {

		std::vector < std::vector < double > > dsMatrix(Ns, vector<double>(Nv, 0));
		// double segmentSize = 1.0/(float)Ns;
		// double curSegment = 0.0;
		int a;
		double c;

		// Create Initial Matrix
		for (int i = 0; i<Ns; i++) {
			for (int j = 0; j<Nv; j++) {
				dsMatrix[i][j] = (i + 1 - 0.5) / Ns;
			}
		}

		// Randomly match
		for (int i = 0; i<Nv; i++) {
			for (int j = 0; j<Ns; j++) {
				a = mt.randInt(Ns - 1);
				c = dsMatrix[j][i];
				dsMatrix[j][i] = dsMatrix[a][i];
				dsMatrix[a][i] = c;
			}
		}

		return dsMatrix;
	}

	// Van der Corput   - Halton in 1 dimension
	// Halton           - Use a consecutvie prime bases for each column
	// Sobol    - Faure, but uses only based 2. Each column is re-ordered based on direction numbers
	std::vector < std::vector < double > > haltonSampling(int Nd, int Ns) {
		std::vector < std::vector < double > > haltonMatrix(Ns, vector<double>(Nd, 0));
		Primes p;
		double curPrime;

		for (int j = 0; j<Nd; j++) {
			curPrime = p.nextPrime();
			for (int i = 0; i<Ns; i++) {
				haltonMatrix[i][j] = Utils::corputBase(curPrime, i + 1);
			}
		}
		return haltonMatrix;
	}

	// Nd - Number of Dimensions
	// Ns - Sample, Zero-based
	std::vector < double > haltonSample(int Nd, int Ns) {
		std::vector < double > haltonSample(Nd, 0.0);
		Primes p;
		double curPrime;

		for (int j = 0; j<Nd; j++) {
			curPrime = p.nextPrime();
			haltonSample[j] = Utils::corputBase(curPrime, Ns + 1);
		}
		return haltonSample;
	}

	std::vector < std::vector < double > > hammersleySampling(int Nd, int Ns) {
		std::vector < std::vector < double > > hMatrix(Ns, vector<double>(Nd, 0.0));
		Primes p;
		double curPrime;

		for (int j = 1; j<Nd; j++) { // Column
			curPrime = p.nextPrime();
			for (int i = 1; i<Ns; i++) { // Row
				hMatrix[i][j] = Utils::corputBase(curPrime, i - 1);
			}
		}
		for (int i = 0; i<Ns; i++) { // Row
			hMatrix[i][0] = i / (double)Ns;
		}
		return hMatrix;
	}

	// Nd - Number of dimensions,
	// Ns - Sample, Zero-based
	std::vector < double > hammersleySample(int Nd, int Ns, int totalSamples) {
		std::vector < double > hSample(Nd, 0.0);
		Primes p;
		double curPrime;

		hSample[0] = Ns / (double)totalSamples;

		for (int i = 1; i<Nd; i++) { // Column
			curPrime = p.nextPrime();
			hSample[i] = Utils::corputBase(curPrime, Ns + 1);
		}

		return hSample;
	}

	std::vector < std::vector < double > > faureSampling(int Nd, int Ns) {
		std::vector < std::vector < double > >  bb(Ns, std::vector<double>(Nd, 0.0));
		std::vector < std::vector < double > >  fMatrix(Ns, std::vector<double>(Nd, 0.0));
		std::vector < std::vector < int > >     a(Ns, std::vector<int>(Nd, 0.0));
		std::vector < std::vector < int > > G;
		Primes prime(Nd - 1);
		double curBase;
		int r;

		curBase = prime.nextPrime();
		r = floor(log((double)Ns) / log(curBase)) + 1;
		G.resize(r, std::vector<int>(r, 0.0));

		for (int j = 0; j<r; j++) {
			for (int i = 0; i<j + 1; i++) {
				G[j][i] = ((int)combination(j, i)) % ((int)curBase);
			}
		}

		for (int i = 0; i<Ns; i++) {
			for (int j = 0; j<Nd; j++) {
				bb[i][j] = 1.0 / pow(curBase, j + 1);
			}
		}

		for (int i = 0; i<Ns; i++) {
			for (int j = 0; j<Nd; j++) {
				a[i][j] = i;
			}
		}

		for (int i = 0; i<(int)a.size(); i++) {
			a[i] = Utils::changeBase(i, curBase, r);
		}

		for (int i = 0; i<Ns; i++) {
			fMatrix[i][0] = corputBase(curBase, i);
		}

		for (int i = 1; i<Nd; i++) {
			a = matMult(a, G);

			for (int row = 0; row<(int)a.size(); row++) {
				for (int col = 0; col<(int)a[row].size(); col++) {
					a[row][col] = a[row][col] % (int)curBase;
				}
			}

			for (int m = 0; m<(int)fMatrix.size(); m++) {
				for (int n = 0; n<r; n++) {
					fMatrix[m][i] += a[m][n] * bb[m][n];
				}
			}
		}

		bb.clear(); a.clear(); G.clear();
		return fMatrix;
	}

	double corputBase(double base, double number) {
		double h, ib;
		double i, n0, n1;

		n0 = number;
		h = 0;
		ib = 1.0 / base;

		while (n0 > 0) {
			n1 = (int)(n0 / base);
			i = n0 - n1 * base;
			h = h + ib * i;
			ib = ib / base;
			n0 = n1;
		}
		return h;
	}

	std::string toLower(std::string str) {
		for (unsigned int i = 0; i<str.length(); i++) {
			str[i] = tolower(str[i]);
		}
		return str;
	}

	std::string toUpper(std::string str) {
		for (unsigned int i = 0; i<str.length(); i++) {
			str[i] = toupper(str[i]);
		}
		return str;
	}

	std::vector<std::string> permuteCharacters(std::string topermute) {

		std::vector<char> iV;
		std::vector<std::string> results;
		std::string s;

		for (unsigned int x = 0; x< topermute.length(); x++) {
			iV.push_back(topermute[x]);
		}

		std::sort(iV.begin(), iV.end());
		s.assign(iV.begin(), iV.end());
		results.push_back(s);

		while (std::next_permutation(iV.begin(), iV.end())) {
			s.assign(iV.begin(), iV.end());
			results.push_back(s);
		}
		return results;
	}

	std::vector<int> changeBase(double num, double base, int numDigits) {
		std::vector<int> retValue;

		do {
			retValue.push_back((int)num % (int)base);
			num = (int)(num / base);
		} while (num != 0);

		while ((int)retValue.size() < numDigits) {
			retValue.push_back(0);
		}

		return retValue;
	}

	std::string changeBase(std::string Base, int number) {
		std::stringstream ss;

		ss << Base << number;
		return ss.str();
	}

	int factorial(int n) {
		int result = 1;

		for (int i = 1; i <= n; ++i) {
			result = result*i;
		}

		return result;
	}
}