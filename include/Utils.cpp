#include "Utils.h"
#include <iostream>
#include "Eigen/Dense"

namespace Utils{

    std::string getPruningMethodString(pruning::PRUNING_METHOD pm){
        std::string retValue;
        switch(pm){
            case pruning::PM_ACO: retValue = "ACO"; break;
            case pruning::PM_AIS: retValue = "AIS"; break;
            case pruning::PM_GA:  retValue = "GA";  break;
            case pruning::PM_MGA: retValue = "MGA"; break;
            case pruning::PM_PSO: retValue = "PSO"; break;
            case pruning::PM_RPSO:retValue = "RPSO";break;
            case pruning::PM_CFO: retValue = "CFO"; break;
            default: retValue = "NONE";
        }
        return retValue;
    }
    std::string getClassificationMethodString(pruning::CLASSIFICATION_METHOD cm){
        std::string retValue;
        switch(cm){
            case pruning::CM_NN:  retValue = "NN";  break;
            case pruning::CM_OPF: retValue = "OPF"; break;
            case pruning::CM_SVM: retValue = "SVM"; break;
            case pruning::CM_CAP: retValue = "CAP"; break;
            default: retValue = "OPF";
        }
        return retValue;
    }
    std::string getSamplingMethodString(pruning::SAMPLING_METHOD ps){
        std::string retValue;
        switch(ps){
            case pruning::SM_ACO:       retValue = "ACO";             break;
            case pruning::SM_AIS:       retValue = "AIS";             break;
            case pruning::SM_GA:        retValue = "GA";              break;
            case pruning::SM_MGA:       retValue = "MGA";             break;
            case pruning::SM_PSO:       retValue = "PSO";             break;
            case pruning::SM_RPSO:      retValue = "RPSO";            break;
            case pruning::SM_MO_PSO:    retValue = "MO_PSO";          break;
            case pruning::SM_LHS:       retValue = "LHS";             break;
            case pruning::SM_DS:        retValue = "DS";              break;
            case pruning::SM_HAL:       retValue = "HAL";             break;
            case pruning::SM_HAM:       retValue = "HAM";             break;
            case pruning::SM_FAU:       retValue = "FAU";             break;
            case pruning::SM_CFO:       retValue = "CFO";             break;
            case pruning::SM_PI:        retValue = "PI";              break;
            case pruning::SM_SOBOL:     retValue = "SOBOL";           break;
            case pruning::SM_ATV:       retValue = "ATV";             break;
            default: retValue = "MCS";
        }
        return retValue;
    }
    std::string getBooleanString(bool value){
        if(value) { return "True";}
        else      { return "False";}
    }
    std::string getPHEVPlacementString(pruning::PHEV_PLACEMENT pp){
        std::string retValue;
        switch(pp){
            case pruning::PP_NONE:              retValue = "NONE";             break;
            case pruning::PP_EVEN_ALL_BUSES:    retValue = "EvenAllBuses";     break;
            case pruning::PP_EVEN_LOAD_BUSES:   retValue = "EvenLoadBuses";    break;
            case pruning::PP_RANDOM_ALL_BUSES:  retValue = "RandomAllBuses";   break;
            case pruning::PP_RANDOM_LOAD_BUSES: retValue = "RandomLoadBuses";  break;
            case pruning::PP_FAIR_DISTRIBUTION: retValue = "FairDistribution"; break;
            default: retValue = "NONE";
        }
        return retValue;
    }
    std::string getPruningObjString(pruning::PRUNING_OBJECTIVE po){
        std::string retValue;
        switch(po){
            case pruning::PO_PROBABILITY :  retValue = "Probability";   break;
            case pruning::PO_CURTAILMENT:   retValue = "Curtailment";   break;
            case pruning::PO_COPY:          retValue = "Copy";          break;
            case pruning::PO_EXCESS:        retValue = "Excess";        break;
            case pruning::PO_ZERO:          retValue = "Zero";          break;
            case pruning::PO_TUP:           retValue = "Tup";           break;
            default: retValue = "Probability";
        }
        return retValue;
    }
    std::string getStoppingMethodString(pruning::STOPPING_METHOD sm){
        std::string retValue;
        switch(sm){
            case pruning::SM_STATES_SLOPE:  retValue = "MinStatesSlope";    break;
            case pruning::SM_STATES_PRUNED: retValue = "MaxStatesPruned";   break;
            case pruning::SM_PROB_MAX:      retValue = "MaxProbPruned";     break;
            case pruning::SM_PROB_SLOPE:    retValue = "MinProbSlope";      break;
            case pruning::SM_GENERATION:    retValue = "Generation";        break;
            default: retValue = "";
        }
        return retValue;
    }

    pruning::PRUNING_METHOD getPruningMethod(std::string s){
        pruning::PRUNING_METHOD retValue;
        s = toUpper(s);
        if(s == "ACO")      {retValue = pruning::PM_ACO;}
        else if(s == "AIS") {retValue = pruning::PM_AIS;}
        else if(s == "GA")  {retValue = pruning::PM_GA;}
        else if(s == "MGA") {retValue = pruning::PM_MGA;}
        else if(s == "PSO") {retValue = pruning::PM_PSO;}
        else if(s == "RPSO"){retValue = pruning::PM_RPSO;}
        else if(s == "CFO") {retValue = pruning::PM_CFO;}
        else                {retValue = pruning::PM_NONE;}
        return retValue;
    }
    pruning::CLASSIFICATION_METHOD getClassificationMethod(std::string s){
        pruning::CLASSIFICATION_METHOD retValue;
        s = toUpper(s);
        if(s == "NN")       {retValue = pruning::CM_NN;}
        else if(s == "OPF") {retValue = pruning::CM_OPF;}
        else if(s == "SVM") {retValue = pruning::CM_SVM;}
        else if(s == "CAP") {retValue = pruning::CM_CAP;}
        else                {retValue = pruning::CM_OPF;}
        return retValue;
    }
    pruning::SAMPLING_METHOD getSamplingMethod(std::string s){
        pruning::SAMPLING_METHOD retValue;
        s = toUpper(s);
        
        if(s == "ACO")                      {retValue = pruning::SM_ACO;}
        else if(s == "AIS")                 {retValue = pruning::SM_AIS;}
        else if(s == "GA")                  {retValue = pruning::SM_GA;}
        else if(s == "MGA")                 {retValue = pruning::SM_MGA;}
        else if(s == "PSO")                 {retValue = pruning::SM_PSO;}
        else if(s == "RPSO")                {retValue = pruning::SM_RPSO;}
        else if(s == "MO_PSO")              {retValue = pruning::SM_MO_PSO;}
        else if(s == "LHS")                 {retValue = pruning::SM_LHS;}
        else if(s == "DS")                  {retValue = pruning::SM_DS;}
        else if(s == "HAL")                 {retValue = pruning::SM_HAL;}
        else if(s == "HAM")                 {retValue = pruning::SM_HAM;}
        else if(s == "FAU")                 {retValue = pruning::SM_FAU;}
        else if(s == "CFO")                 {retValue = pruning::SM_CFO;}
        else if(s == "PI")                  {retValue = pruning::SM_PI;}
        else if(s == "SOB")                 {retValue = pruning::SM_SOBOL;}
        else if(s == "ATV")                 {retValue = pruning::SM_ATV;}
        else                                {retValue = pruning::SM_MCS;}
        return retValue;
    }
    pruning::PHEV_PLACEMENT getPHEVPlacement(std::string s){
        pruning::PHEV_PLACEMENT retValue;
        s = toUpper(s);
        if(s == "EVENALLBUSES")         {retValue = pruning::PP_EVEN_ALL_BUSES;}
        else if(s == "EVENLOADBUSES")   {retValue = pruning::PP_EVEN_LOAD_BUSES;}
        else if(s == "RANDOMALLBUSES")  {retValue = pruning::PP_RANDOM_ALL_BUSES;}
        else if(s == "RANDOMLOADBUSES") {retValue = pruning::PP_RANDOM_LOAD_BUSES;}
        else if(s == "FAIRDISTRIBUTION"){retValue = pruning::PP_FAIR_DISTRIBUTION;}
        else                            {retValue = pruning::PP_NONE;}
        return retValue;
    }
    pruning::STOPPING_METHOD getStoppingMethod(std::string s){
        pruning::STOPPING_METHOD retValue;
        s = toUpper(s);
        if(s == "MINSTATESSLOPE")       {retValue = pruning::SM_STATES_SLOPE;}
        else if(s == "MAXSTATESPRUNED") {retValue = pruning::SM_STATES_PRUNED;}
        else if(s == "MAXPROBPRUNED")   {retValue = pruning::SM_PROB_MAX;}
        else if(s == "MINPROBSLOPE")    {retValue = pruning::SM_PROB_SLOPE;}
        else if(s == "GENERATION")      {retValue = pruning::SM_GENERATION;}
        else                            {retValue = pruning::SM_GENERATION;}
        return retValue;
    }
    pruning::PRUNING_OBJECTIVE getPruningObj(std::string s){
        pruning::PRUNING_OBJECTIVE retValue;
        s = toUpper(s);
        if(s == "PROBABILITY")      {retValue = pruning::PO_PROBABILITY;}
        else if(s == "PROB")        {retValue = pruning::PO_PROBABILITY;}
        else if(s == "CURTAILMENT") {retValue = pruning::PO_CURTAILMENT;}
        else if(s == "CURT")        {retValue = pruning::PO_CURTAILMENT;}
        else if(s == "COPY")        {retValue = pruning::PO_COPY;}
        else if(s == "EXCESS")      {retValue = pruning::PO_EXCESS;}
        else if(s == "ZERO")        {retValue = pruning::PO_ZERO;}
        else if(s == "TUP")         {retValue = pruning::PO_TUP;}
        else                        {retValue = pruning::PO_PROBABILITY;}
        
        return retValue;
    }
}
