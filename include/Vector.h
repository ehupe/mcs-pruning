#ifndef VECTOR_H_
#define VECTOR_H_

#include <iomanip>
#include <iterator>
#include <math.h>
#include <sstream>
#include <stdio.h>
#include <string>
#include <vector>

#ifndef _OPENMP
#include "omp.h"
#endif

#include "anyoption.h"
#include "Bus.h"
#include "defs.h"
#include "Generator.h"
#include "Line.h"
#include "MTRand.h"
#include "Primes.h"
#include "RandomNumbers.h"

namespace Vector {

	extern void printVector(std::vector < std::vector < double > >& v, std::string title, int precision = 2);
	extern void printVector(std::vector < std::vector < std::vector < double > > >& v, std::string title, int precision = 2);
};
#endif
